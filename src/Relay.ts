import { platform } from "os";
import { Data, WebSocket } from "ws";
import { IPty, spawn } from "node-pty";
import { randomUUID } from "crypto";

const shell = platform() === 'win32' ? 'powershell.exe' : 'bash';

type TerminalInstance = 
      { uuid: string, hasTerminal: boolean, terminal?: IPty } 
    | { uuid: string, hasTerminal: true, terminal: IPty }; 

export class OdinRelayClient {

    private ws: WebSocket;
    private __connected = false;
    private __instances: TerminalInstance[] = [];
    private uuid: string = randomUUID();

    constructor (private ws_url: string) {
        
        this.ws = new WebSocket(ws_url);
        this.__hookWS();

    }

    private __hookWS () {

        this.ws.onerror = e => {
            console.log("Odin Relay client WS Error: ", e);
        };

        this.ws.onclose = () => {
            this.__connected = false;
            console.error("Server disconnected, trying to reconnect in 5s");
            setTimeout(() => {
                this.ws = new WebSocket(this.ws_url);
                this.__hookWS();
            }, 5000);
            this.__instances.forEach(i => i.terminal?.kill());
        }

        this.ws.onopen = () => {
            this.__connected = true;
            this.__sendHello();
            this.__sendAttach();
            console.log("Relay client connected");
        }

        this.ws.onmessage = msg => {
            this.handle(msg.data);
        }
    }

    handle (raw: Data) {
        let payload: OdinPayload<RelayMessage | ServerHello>;
        
        try {
            payload = JSON.parse(raw.toString());
        
            // Checks payload structure
            if (!payload.__data || !payload.__message || !payload.__sender) {
                throw "Invalid Odin Payload !";
            }

            if ("type" in payload.__data && payload.__data.type == "hello") {
                this.uuid = payload.__data.uuid;
                return;
            }

            if (!("event" in payload.__data)) {
                throw "Invalid Relay Payload !";
            }

        } catch (e) {
            console.error("Parsing Error on message Handle: \n", e);
            return;
        }

        console.log("<|", payload.__sender.name, "|", payload.__data.event);

        switch (payload.__data.event) {
            case "connect":
                this.__clientConnect(payload.__data.uuid);
                break;
            case "disconnect":
                this.__clientDisconnect(payload.__data.uuid);
                break;
            case "rawTerm":
                this.__clientRawTerm(payload.__sender.name, payload.__data.data);
                break;
            case "request":
                this.__clientRequest(<OdinPayload<RelayRequest>>payload);
                break;
            case "response":
                // Blk frr
                break;

            default:
                console.warn(`Unknown Event from ${payload.__sender.name} :\n`, payload);
        }

    }

    private __clientConnect (uuid: string) {
        this.__instances.push({ uuid, hasTerminal: false });
    }

    private __clientDisconnect (uuid: string) {
        let client = this.__getClient(uuid);
        this.__instances = this.__instances.filter(i => i.uuid != uuid);
        
        if (!client) return;
        if (!client.hasTerminal) return;

        client.terminal?.kill();
    }

    private __getClient (uuid: string): TerminalInstance | undefined {
        return this.__instances.filter(i => i.uuid == uuid)[0];
    }

    private __clientRequest (req: OdinPayload<RelayRequest>) {

        switch (req.__data.req) {
            case 'terminal':
                this.__clientRequestNewTerm(req);
                break;

            case 'resize':
                this.__clientRequestResizeTerm(req);
                break;

            case 'discover':
                this.__sendDiscover(req);
                break;

            default:
                console.warn(`Unknown request from ${req.__sender.name} '${req.__data.req}'`);
                this.__respond<string>(req.__sender.name, req.__message.uid, "Unknown request", true);
                break;

        }

    }

    private __clientRequestNewTerm (req: OdinPayload<RelayRequest>) {

        let client = this.__getClient(req.__sender.name);

        if (!client) {
            console.error(`Can't create terminal for unknown user ${req.__sender.name}`);
            this.__respond<string>(req.__sender.name, req.__message.uid, "Unknown User", true);
            return;
        }

        if (client.hasTerminal) {
            console.error(`User ${req.__sender.name} has aleready a terminal`);
            this.__respond<string>(req.__sender.name, req.__message.uid, "Already Exists", true);
            return;
        }

        client.hasTerminal = true;
        client.terminal = spawn(shell, [], {
            name: 'xterm-color',
            cols: 80,
            rows: 30,
            cwd: process.env.HOME,
            env: <{ [key: string]: string }>process.env
        });

        client.terminal.onData(data => {
            if (!client) return;
            this.__sendRawTerm(client.uuid, data);
        });

        this.__respond<string>(req.__sender.name, req.__message.uid, "OK");

    }

    private __clientRequestResizeTerm (req: OdinPayload<RelayRequest>) {
        let client = this.__getClient(req.__sender.name);
        if (!client || !client.hasTerminal) {            
            console.error(`User ${req.__sender.name} doesn't have a terminal`);
            this.__respond<string>(req.__sender.name, req.__message.uid, "No Terminal", true);
            return;
        };

        if (!req.__data.args || req.__data.args.length < 2) {
            console.error(`User ${req.__sender.name} has not provided enough arguments`);
            this.__respond<string>(req.__sender.name, req.__message.uid, "Not Enough Arguments", true);
            return;
        }

        console.log(req.__data.args.map(parseInt));
        let [ c, r ] = req.__data.args.map(e => parseInt(e));
        c ||= 80;
        r ||= 30;

        client.terminal?.resize(c, r);
    }

    private __sendDiscover (req: OdinPayload<RelayRequest>) {
        let payload = <OdinPayload<RelayDicover>>this.__emptyPayload(req.__sender.name);
        payload.__data = { event: "discover", uuid: this.uuid };
        this.__send(payload);
    }

    private __clientRawTerm (uuid: string, data: string) {
        let client = this.__getClient(uuid);
        if (!client || !client.hasTerminal) return;
        client.terminal?.write(data);
    }

    private __respond <T>(to: string, uid: string, data: T, error?: true) {
        let payload: OdinPayload<RelayResponse<T>> = this.__emptyPayload(to, uid);
        payload.__data = {
            event: "response", value: data, error
        };
        this.__send(payload);
    } 

    private __sendRawTerm (to: string, data: string) {
        let payload: OdinPayload<RelayRawTerminal> = this.__emptyPayload(to);
        payload.__data = { event: "rawTerm", data };
        this.__send(payload);
    }

    private __sendHello () {
        let payload: OdinPayload<ClientHello> = this.__emptyPayload("osef");
        payload.__data = { type: "hello" };
        payload.__sender.type = "client";
        this.__send(payload);
    }
    private __sendAttach () {
        let payload: OdinPayload<ClientAttachRelay> = this.__emptyPayload("osef");
        payload.__data = { type: "relay.attach" };
        payload.__sender.type = "client";
        this.__send(payload);
    }

    private __send (data: OdinPayload<any>) {
        if (!this.__connected) {
            console.log("Tried sending message while disconnected");
            return;
        }
        this.ws.send(JSON.stringify(data));
    }

    private __emptyPayload (to: string, uid?: string): OdinPayload<any> {
        return {
            __message: { for: to, uid: uid || randomUUID(), kind: (uid && "response") || undefined },
            __data: undefined,
            __sender: { via: [ this.uuid ], name: this.uuid, type: "relay" },
        }
    }

}

/* Volontairement re-défini pour partager ce fichier en dehors d'ici */
type OdinPayload<T> = {
    __sender: { 
        type: "client" | 'server' | "relay";
        name: string;
        via: string[];
    };
    __message: {
        kind?: 'response' | 'redirect';
        uid: string;
        for: string;
    };
    __data: T;
};

type RelayMessage = 
      RelayClientConnect 
    | RelayClientDisconnect
    | RelayResponse<any>
    | RelayRequest
    | RelayRawTerminal
    | RelayDicover;

type RelayResponse<T> = {
    event: 'response', error?: true, value: T
}

type RelayRequest = {
    event: 'request', req: string, args: string[]
}

type RelayRawTerminal = {
    event: 'rawTerm', data: string
}

type RelayClientConnect = {
    event: "connect", uuid: string
}

type RelayClientDisconnect = {
    event: "disconnect", uuid: string
}

type RelayDicover = {
    event: "discover", uuid: string
}

type ServerHello = { 
    type: "hello";
    uuid: string;
    srvUid: string;
}
type ClientHello = {
    type: "hello";
}
