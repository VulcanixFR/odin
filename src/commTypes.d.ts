/* Wrapper Global */
type OdinPayload<T> = {
    __sender: { 
        type: "client" | 'server' | "relay";
        name: string;
        via: string[];
    };
    __message: {
        kind?: 'response' | 'redirect';
        uid: string;
        for: string;
    };
    __data: T;
};

/* Types spécifiques */
type SrvMsgs = ServerHello;
type CltMsgs = ClientHello | ClientAttachRelay;

type ServerHello = { 
    type: "hello";
    uuid: string;
    srvUid: string;
}

type ClientHello = {
    type: "hello";
}

type ClientAttachRelay = {
    type: "relay.attach"
}