import { config } from "dotenv";
import express from "express";
import http from "http";
import { WebSocketServer } from "ws";
import { Client } from "./client.js";
import { OdinRelayServer } from "./RelayServer.js";
import os from "os";
import { OdinRelayClient } from "./Relay.js";

// Env Vars
config();

const HOST = process.env.HOST || os.hostname();
const PORT = parseInt(process.env.PORT || "12042");
const URL = `${HOST}:${PORT}`;

const app = express();
const srv = http.createServer(app);
const wss = new WebSocketServer({ server: srv });
const relaySrv = new OdinRelayServer(URL);

const connexions: Client[] = [];

app.use(express.static("public/dist"));

wss.on("connection", (ws) => {

    let iencli = new Client(ws, URL);
    connexions.push(iencli);

    if (process.env.ODIN_RELAY_SRV == "yes") {

        iencli.onAttachRelay = () => {
            relaySrv.subscribe(iencli);
            return true;
        }
        iencli.onDetachRelay = () => {
            relaySrv.unsubscribe(iencli);
        }
        iencli.onRelay = (data) => {
            relaySrv.handle(data);
        }

    }

})

srv.listen(PORT, () => console.log("Serveur Up !", URL));

/** Client Relais Odin si var d'env **/
if (process.env.ODIN_RELAY_HOST) {

    let relayClient = new OdinRelayClient(process.env.ODIN_RELAY_HOST);

}