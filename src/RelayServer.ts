import { Client } from "./client";
import { randomUUID } from "crypto";

export class OdinRelayServer {

    private __clients: Client[] = [];

    constructor (private url: string) {

    }

    // Attaches client to broadcast list
    subscribe (client: Client) {
        this.__clients.push(client);
        this.broadcast<RelayClientConnect>({
            __data: { event: "connect", uuid: client.uuid },
            __message: { for: "*", uid: randomUUID() },
            __sender: { name: this.url, type: "relay", via: [  ] }
        });
        console.log("Attahched new relay client", client.uuid);
    }

    // Detaches client from broadcast list
    unsubscribe (client: Client) {
        this.__clients = this.__clients.filter(c => c != client);
        this.broadcast<RelayClientDisconnect>({
            __data: { event: "disconnect", uuid: client.uuid },
            __message: { for: "*", uid: randomUUID() },
            __sender: { name: this.url, type: "relay", via: [  ] }
        });
        console.log("Detached relay client", client.uuid);
    }

    // Relays data to the whole broadcast list
    broadcast <T>(data: OdinPayload<T>) {
        // Not relaying data sent by self
        if (data.__sender.via.indexOf(this.url) != -1) return;

        // Adding self to relay list
        data.__sender.via.push(this.url);

        console.log("[INFO] Broadcasting message =>", data.__message.uid);

        for (let c of this.__clients) {
            c.relay(data);
        }
    }

    // Decides how to manage a payload
    handle (data: OdinPayload<any>) {

        let iencli = this.__clients.filter(c => c.uuid == data.__message.for)[0];
        
        // Message has to be broadcasted
        if (!iencli) {
            this.broadcast(data);
            return;
        }

        // Adding self to relay list
        data.__sender.via.push(this.url);
        
        console.log(`[INFO] Redirecting message [${data.__message.uid}] to client (${iencli.name != "N/A" ? iencli.name : iencli.uuid})`);

        iencli.relay(data);

    }

}

type RelayClientConnect = {
    event: "connect", uuid: string
}

type RelayClientDisconnect = {
    event: "disconnect", uuid: string
}
