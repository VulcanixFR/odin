import WebSocket, { RawData } from "ws";
import { randomUUID } from "node:crypto";

export class Client {

    readonly uuid = randomUUID();
    private __name = "N/A";
    private connexion = false;

    public onRelay: (payload: OdinPayload<any>) => void = () => {};
    public onAttachRelay: () => boolean = () => false;
    public onDetachRelay: () => void = () => {};

    constructor (private ws: WebSocket, private srvUrl: string) { 
        
        ws.on("message", (data) => {
            this.handleMessage(data);
        })

        ws.on("close", () => {
            this.onDetachRelay();
        })

    }

    private handleMessage (raw: RawData) {
        let payload: OdinPayload<CltMsgs>;
        
        try {
            payload = JSON.parse(raw.toString());
        
            // Checks payload structure
            if (!payload.__data || !payload.__message || !payload.__sender) 
                throw "Invalid Odin Payload !";

        } catch (e) {
            console.error("Parsing Error on message Handle: \n", e);
            return;
        }

        if (payload.__sender.type == "relay") {
            console.log("|<", payload.__sender.name)
            this.onRelay(payload);            
            return;
        }        

        let data = payload.__data;

        console.log('<<', data)
        
        switch (data.type) {
            case "hello":
                this.connexion = true;
                this.sayHello(payload.__message.uid);
                break;
            case "relay.attach":
                this.onAttachRelay();
                break;

            default:
                console.log(data);
        }

    }

    // Relays an Odin Payload
    relay (payload: OdinPayload<any>) {
        try {
            this.ws.send(JSON.stringify(payload));
        } catch (e) {
            console.error("Stringify Error while relaying: \n", e);
        }
    }

    // Transforms any data into Odin payload and sends it
    private send <T>(payload: T, header: OdinPayload<T>["__message"]): boolean {
        if (!this.connexion) return false;
        console.log(">>", payload)

        let data: OdinPayload<T> = {
            __data: payload,
            __message: header,
            __sender: this.sender
        };

        this.ws.send(JSON.stringify(data));
        return true;

    }

    private sayHello (uid: string) {
        this.send<ServerHello>({ type: "hello", uuid: this.uuid, srvUid: this.srvUrl }, { for: this.uuid, uid, kind: "response" });
    }

    get sender (): OdinPayload<any>["__sender"] {
        return {
            name: this.srvUrl,
            type: "server",
            via: [ this.srvUrl ]
        };
    }

    get name () {
        return this.__name;
    }

}