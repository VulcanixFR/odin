import { defineConfig } from "vite";

export default defineConfig({
    base: '',
    root: ".",
    publicDir: "assets",
    build: {
        rollupOptions: {
            input: {
                main: "index.html",
                terminal: "terminal.html"
            }
        }
    }
})