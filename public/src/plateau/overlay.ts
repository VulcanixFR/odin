import { GATEAU, Gateau } from "./gateau";
import { Objet } from "./objet";
import { EQUIPE, Robot } from "./robtot";

type overlay = {
    elem: HTMLSpanElement;
    obj: Objet;
    update: (elem: HTMLSpanElement, o: Objet) => void;
    visible: boolean;
};

export class Overlay {

    private etat: {
        mode: 'hover' | 'toggle' | 'always' | 'never';
    } = {
        mode: 'hover'
    };
    private overlays: overlay[] = [];

    constructor (public readonly elem: HTMLElement) {

    }

    add (obj: Objet): void {
        if (this.find(obj)) return; // Existe déjà

        let elem = document.createElement("span");
        elem.className = "objet info hidden";

        if (obj.objType == "gateau") {
            let G = <Gateau>obj;
            
            let update = (elem: HTMLSpanElement, o: Objet) => {  
                let G = <Gateau>o;
                let couleur = G.type == GATEAU.INCONNU ? 'grey' : ( G.type == GATEAU.MARRON ? 'brown' : (G.type == GATEAU.JAUNE ? 'gold' : "magenta") );
                elem.style.setProperty("--couleur", couleur);
                elem.innerHTML = Overlay.dataToLi([ { titre: "X", data: G.x }, { titre: "Y", data: G.y } ]);
            };
            update(elem, G);

            this.overlays.push({ elem, obj, update, visible: false }); 
        }

        else if (obj.objType == "robot") {
            let robot = <Robot>obj;
            
            let update = (elem: HTMLSpanElement, o: Objet) => {  
                let R = <Robot>o;
                let couleur = R.equipe == EQUIPE.BLEUE ? "blue" : "green";
                elem.style.setProperty("--couleur", couleur);
                elem.innerHTML = Overlay.dataToLi([ {titre: "Nom", data: R.nom}, { titre: "X", data: R.x }, { titre: "Y", data: R.y }, { titre: "&alpha;", data: ~~(180 * R.angle / Math.PI) + "°" } ]);
            };
            update(elem, robot);

            this.overlays.push({ elem, obj, update, visible: false });      
        }

        Overlay.setPos(elem, obj.x, obj.y);
        this.elem.appendChild(elem);
        this.render();
    }

    rm (obj: Objet): void {
        let overlay = this.find(obj);
        if (!overlay) return;
        overlay.elem.remove();
        this.overlays = this.overlays.filter(e => e.obj != obj);
    }

    find (obj: Objet) {
        return this.overlays.filter(e => e.obj == obj)[0];
    }

    toggle (obj: Objet) {
        let overlay = this.find(obj);
        if (!overlay) return;
        overlay.visible = !overlay.visible;
    }

    render () {
        for (let overlay of this.overlays) {
            let visible = overlay.visible;
            if (this.mode == "always") visible = true; 
            else if (this.mode == "hover") visible = overlay.obj.hover;
            else if (this.mode == "never") visible = false;

            overlay.visible = visible;
            overlay.elem.classList.toggle("hidden", !visible);
            
            overlay.update(overlay.elem, overlay.obj);

            Overlay.setPos(overlay.elem, overlay.obj.x, overlay.obj.y);
        }
    }

    get mode () {
        return this.etat.mode;
    }
    set mode (m) {
        this.etat.mode = m;
        this.render();
    }

    static dataToLi (data: {titre: string, data: string | number}[]): string {
        return `<ul>${data.map(e => Overlay.format(e.titre, e.data)).join("")}</ul>`;
    }

    static format (titre: string, data: string | number): string {
        return `<li>${titre}: ${typeof data == "number" ? data.toFixed(2) : data}</li>`;
    }

    static setPos (elem: HTMLElement, x: number, y: number) {
        elem.style.setProperty("--x", x.toFixed(2));
        elem.style.setProperty("--y", y.toFixed(2));
    }

}