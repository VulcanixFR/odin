export class Objet {
    
    private etat: { 
        x: number, y: number, angle: number, user_ctrl: boolean,
        souris: { hover: boolean, active: boolean }
    } = { 
        x: 0, y: 0, angle: 0, user_ctrl: false,
        souris: { active: false, hover: false }
    };
    
    public readonly elem: HTMLSpanElement;

    constructor (public readonly rayon: number, public readonly objType: string = "none") {

        this.elem = document.createElement("span");
        this.elem.className = "objet"
        this.elem.addEventListener("mouseenter", e => this.etat.souris.hover = true);
        this.elem.addEventListener("mouseleave", e => this.etat.souris.hover = false);
        this.elem.addEventListener("mousedown",  e => this.etat.souris.active = true);
        this.elem.addEventListener("mouseup",    e => this.etat.souris.active = false);

    }  

    remove (): void { this.elem.remove() }
 
    bouge (dx: number, dy: number): void {
        this.x += (dx * Math.cos(this.etat.angle) + dy * Math.sin(this.etat.angle));
        this.y += (dy * Math.cos(this.etat.angle) - dx * Math.sin(this.etat.angle));
    }

    get x () { return this.etat.x }
    get y () { return this.etat.y }
    get pos () { 
        return { 
            x: this.etat.x, y: this.etat.y
        };
    }
    get angle () { return this.etat.angle }
    get user_ctrl () { return this.etat.user_ctrl }

    set x (x) {
        x = Math.min(3000 - this.rayon, Math.max(this.rayon, x));
        this.etat.x = x;
        this.elem.style.setProperty("--x", x.toFixed(2));
    }
    set y (y) {
        y = Math.min(2000 - this.rayon, Math.max(this.rayon, y));
        this.etat.y = y;
        this.elem.style.setProperty("--y", y.toFixed(2));
    }
    set pos (p) {
        let { x, y } = p;
        this.x = x;
        this.y = y;
    }
    set angle (alpha) {
        this.etat.angle = (alpha + 2 * Math.PI) % (Math.PI * 2);
        this.elem.style.setProperty("--alpha", this.etat.angle.toFixed(2) + "rad");
    }
    set user_ctrl (c) {
        this.etat.user_ctrl = c;
        this.elem.classList.toggle("ctrl", c);
    }

    get hover () {
        return this.etat.souris.hover;
    }
    get active () {  
        return this.etat.souris.active;
    }

}