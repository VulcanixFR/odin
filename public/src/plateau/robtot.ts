import { Objet } from "./objet";

export const enum EQUIPE { BLEUE, VERTE };

export class Robot extends Objet {

    public equipe: EQUIPE = EQUIPE.BLEUE;

    constructor (public readonly nom: string) {
        super(190, "robot");
        this.elem.classList.add("robot");
    }

}