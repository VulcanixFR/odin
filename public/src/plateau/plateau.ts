import { Gateau, GATEAU } from "./gateau";
import { Objet } from "./objet";
import { Overlay } from "./overlay";
import { Robot } from "./robtot";

export type descriptionGateau = {
    type: 'gato', 
    uid: number, 
    opt: "jaune" | "rose" | "marron" | "inconnu", 
    x: number, y: number, alpha: number 
};
export type descriptionRobot = {
    type: 'robot', 
    uid: number, 
    opt: "1" | "2" | "3" | "4", 
    x: number, y: number, alpha: number 
};
export type descriptionCerise = {
    type: 'cerise', 
    uid: number, 
    opt: "0", 
    x: number, y: number, alpha: number 
};

export type descriptionObjet = descriptionGateau | descriptionCerise | descriptionRobot;

export class Plateau {

    private html: {
        plateau: HTMLDivElement,
        couches: { 
            gateau: HTMLDivElement,
            robots: HTMLDivElement,
            commande: HTMLDivElement,
            overlay: HTMLDivElement,
        }
    };

    private etat: {
        vertical: boolean, ratio: number, echelle: number,
        override: boolean, angle: number, translation: { x: number, y: number },
        majVisuel: boolean, focus: Objet | undefined, render: boolean,
    };

    private gateaux: Gateau[];
    private robots: Robot[];
    public overlay: Overlay;

    constructor (plateau: HTMLDivElement) {

        this.html = {
            plateau,
            couches: { 
                gateau: <HTMLDivElement>plateau.querySelector(".gateau"),
                robots: <HTMLDivElement>plateau.querySelector(".robot-reel"),
                commande: <HTMLDivElement>plateau.querySelector(".robot-ideal"),
                overlay: <HTMLDivElement>plateau.querySelector(".overlay")
            },
        };
        this.gateaux = [];
        this.robots = [];

        this.etat = {
            vertical: false, echelle: 0.1, ratio: 1.5, 
            override: false, angle: 0, translation: { x: 0, y: 0 },
            majVisuel: false, focus: undefined, render: false
        };

        this.overlay = new Overlay(this.html.couches.overlay);

        this.init_listeners();
        this.resize();
        this.startRender();
    }

    init_listeners (): void {
        window.addEventListener("resize", () => {
            this.resize();
        })
    }

    /**
     * Adapte le plateau pour la taille de la fenêtre
     */
    resize (): void {
        this.vertical = window.innerHeight > window.innerWidth;
        this.ratio = this.vertical ? window.innerHeight / window.innerWidth : window.innerWidth / window.innerHeight;
        this.echelle = this.vertical ? ( this.ratio < 1.5 ? window.innerHeight / 3000 : window.innerWidth / 2000 ) : ( this.ratio < 1.5 ? window.innerWidth / 3000 : window.innerHeight / 2000 );
    }

    /**
     * Ajoute un Gateau sur le plateau (dans sa span correspondante)
     */
    ajoutGateau (uid: number, type: GATEAU): Gateau {
        let gat = this.gateau(uid);
        if (!gat) { 
            gat = new Gateau(uid, type);
            this.gateaux.push(gat);
        } else gat.type = type;

        this.html.couches.gateau.appendChild(gat.elem);
        this.overlay.add(gat);

        return gat;
    }

    /**
     * Supprime un Gateau
     */
    supprimeGateau (uid: number): void {
        let gat: Gateau | undefined;
        gat = this.gateaux.filter(e => e.uid == uid)[0];
        if (gat) {
            this.overlay.rm(gat);
            gat.remove();
            this.gateaux = this.gateaux.filter(e => e.uid != uid);
        }
    }

    /**
     * Trouve un gateau et le retourne
     */
    gateau (uid: number): Gateau | undefined {
        return this.gateaux.filter(e => e.uid == uid)[0];
    }

    /**
     * Transforme le point de vue du plateau sur la position indiquée (en mm)
     */
    focusPoint (x: number, y: number, alpha: number = 0, echelle?: number): void {
        if (this.etat.focus) return; // Priorité au suivi d'objet

        this.override = true;
        this.angle = alpha;
        this.translation = { x, y };
        if (echelle) this.echelle = echelle;
    }

    /**
     * Centre la "caméra" sur l'objet souhaité
     */
    focus (obj: Objet): void {
        this.etat.focus = obj;
        setTimeout(() => this.override ? this.html.plateau.classList.add("noanim") : undefined, 400);
        this.startRender();
    }

    /**
     * Arrête tout focus
     */
    stopFocus (): void {
        this.override = false;
        this.etat.focus = undefined;
        this.angle = 0;
        this.translation = { x: 0, y: 0 };
        this.html.plateau.classList.remove("noanim")
        this.resize()
    }

    /**
     * Démarre le rendu
     */
    startRender () {
        if (this.etat.render) return;
        this.etat.majVisuel = true;
        this.majVisuel();
    }

    /**
     * Met à jour le visuel du plateau
     */
    private majVisuel () {
        if (!this.etat.majVisuel) {
            this.etat.render = false;
            return;
        };

        this.etat.render = true;

        this.overlay.render();

        if (this.etat.focus) {
            let obj = this.etat.focus;
            this.override = true;
            this.translation = obj.pos;
            this.angle = (-Math.PI / 2) -obj.angle;
        }

        window.requestAnimationFrame(() => this.majVisuel());
    }

    /**
     * Ajoute un robot sur le plateau
     */
    ajouteRobot (r: Robot) {
        let index = this.robots.push(r) - 1;
        this.html.couches.robots.append(r.elem);
        this.overlay.add(r);
        return index;
    }

    /**
     * Retourne le robot n -- Hautement temporaire
     */
    robot (n: number) {
        return this.robots[n];
    }

    /**
     * Charge les objets en masse
     */
    chargeDescriptions (objets: descriptionObjet[]): void {

        for (let gateau of this.gateaux) {
            gateau.oubli ++;
        }

        for (let d of objets) {

            switch (d.type) {
                case 'gato':
                    let G = this.gateau(d.uid);
                    if (!G) {
                        G = this.ajoutGateau(d.uid, GATEAU.INCONNU);
                    }                
                    G.pos = { x: d.x, y: d.y };
                    G.angle = d.alpha;
                    G.type = d.opt == "inconnu" ? GATEAU.INCONNU : ( d.opt == 'jaune' ? GATEAU.JAUNE : ( d.opt == 'marron' ? GATEAU.MARRON : GATEAU.ROSE ));
                    G.oubli = 0;
                    break;
                case "robot":
                    let R = this.robot(parseInt(d.opt) - 1); // TEMP
                    R.pos = { x: d.x, y: d.y };
                    R.angle = d.alpha;
                    break;
                case 'cerise':

                    break;
                default:
                    // Ne fait rien :D
            }

        }

        for (let gateau of this.gateaux) {
            if (gateau.oubli >= 5) {
                this.supprimeGateau(gateau.uid);
            }
        }

    }

    /**
     * Getters & Setters
     */
    
    get vertical () { return this.etat.vertical; }
    set vertical (status) {
        if (status != this.etat.vertical)
            this.html.plateau.classList.toggle("vertical", status);
        this.etat.vertical = status;
    }

    get ratio () { return this.etat.ratio; }
    set ratio (ratio) { this.etat.ratio = ratio; }

    get echelle () { return this.etat.echelle }
    set echelle (echelle) {
        this.etat.echelle = Math.min(1, Math.max(echelle, 0.1));
        this.html.plateau.style.setProperty("--echelle", this.etat.echelle.toString());
    }

    get angle () { return this.etat.override ? ( this.etat.angle ) : ( this.etat.vertical ? Math.PI/2 : 0 ); }
    set angle (angle) {
        this.etat.angle = angle;
        this.html.plateau.style.setProperty("--angle", angle.toFixed(2) + "rad");
    }

    get override () { return this.etat.override; }
    set override (o) {
        this.etat.override = o;
        this.html.plateau.classList.toggle("override", o);
        if (o) { this.angle *= 1; }
        // else { this.etat.majVisuel = false; }
    }

    get translation () { return this.etat.translation; }
    set translation (tr) {
        this.etat.translation = tr;
        this.html.plateau.style.setProperty("--dx", (tr.x - 1500).toFixed(2));
        this.html.plateau.style.setProperty("--dy", (tr.y - 1000).toFixed(2));
    }

    get focused () {
        return this.etat.focus;
    }

}