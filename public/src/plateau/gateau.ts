import { Objet } from "./objet";

export const enum GATEAU { INCONNU, JAUNE, ROSE, MARRON };

export class Gateau extends Objet {
    
    private _oubli: number = 0;   
    private _type: GATEAU = GATEAU.INCONNU; 

    constructor (public readonly uid: number, type: GATEAU, conteneur?: HTMLDivElement) {

        super(60, "gateau");

        this.elem.classList.add("gateau");
        this.elem.dataset.id = uid.toString();
        this.type = type;

        if (conteneur) conteneur.appendChild(this.elem);

    }

    get oubli () { return this._oubli }
    set oubli (o) {
        if (0 > o || o > 5) return;
        this._oubli = o;
        this.elem.style.setProperty("--oubli", o.toString());
    }

    get type () {
        return this._type;
    }
    set type (t) {
        this._type = t;
        this.elem.classList.toggle("marron", t == GATEAU.MARRON);
        this.elem.classList.toggle("jaune", t == GATEAU.JAUNE);
        this.elem.classList.toggle("rose", t == GATEAU.ROSE);
    }

}