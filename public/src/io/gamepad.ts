type bit = 1 | 0;

export type GamepadButtons = {
    menu:       bit, 
    options:    bit,    
    joystick:   { L: bit, R: bit },  
    dPad:       { up: bit, down: bit, right: bit, left: bit },
    bumper:     { L: bit, R: bit },
    user:       { A: bit, B: bit, X: bit, Y: bit };
};


export type GamepadState = {
    dx: number, dy: number,
    dalpha: number, throttle: number,
    buttons: GamepadButtons   
}

export class GamepadInput extends EventTarget {
 
    /* Paramètres */
    public params: {
        positionAccel: number;
        zoomAccel: number;
        rotationAccel: number;
        gamepadType: 'xboxX' | 'ps4';

        deadZoneR: number;
        deadZoneL: number;

        xDirection: 1 | -1;
        yDirection: 1 | -1;
        alphaDirection: 1 | -1;

        updatePeriod: number; // en ms
    }

    /* Variables internes */
    private raw: Gamepad | null = null;
    
    private prev_btns: GamepadButtons;

    private disconnectListener: (e: GamepadEvent) => void;
    private lastUpdate: number = Date.now();

    constructor (public readonly padIndex: number) {
        super();

        this.disconnectListener = e => {
            if (e.gamepad.index == this.padIndex) this.__disconnect();
        }

        window.addEventListener("gamepaddisconnected", this.disconnectListener);

        this.params = {
            gamepadType: "xboxX",
            positionAccel: 1, zoomAccel: 1, rotationAccel: 1, 
            deadZoneL: 0.1, deadZoneR: 0.1, 
            xDirection: 1, yDirection: 1, alphaDirection: 1,
            updatePeriod: 0,
        };

        this.fetch();
        this.prev_btns = this.getButtons();

    }

    // Met à jour les données de la manette
    private fetch () {
        let gp = navigator.getGamepads()[this.padIndex];
        if (!gp) {
            this.__disconnect();
            return;
        }
        this.raw = gp;
    }

    private getButtons (): GamepadButtons {
        if (!this.raw) throw "Ne peut pas lire une manette inexistante";
        return {
            menu: (this.raw.buttons[9].pressed && 1) || 0,
            options: (this.raw.buttons[8].pressed && 1) || 0,
            joystick: {
                L: (this.raw.buttons[10].pressed && 1) || 0,
                R: (this.raw.buttons[11].pressed && 1) || 0,
            },
            bumper: {
                L: (this.raw.buttons[4].pressed && 1) || 0,
                R: (this.raw.buttons[5].pressed && 1) || 0,
            },
            dPad: {
                up: (this.raw.buttons[12].pressed && 1) || 0,
                down: (this.raw.buttons[13].pressed && 1) || 0,
                left: (this.raw.buttons[14].pressed && 1) || 0,
                right: (this.raw.buttons[15].pressed && 1) || 0,
            },
            user: {
                A: (this.raw.buttons[0].pressed && 1) || 0,
                B: (this.raw.buttons[1].pressed && 1) || 0,
                X: (this.raw.buttons[2].pressed && 1) || 0,
                Y: (this.raw.buttons[3].pressed && 1) || 0,
            }
        }
    }

    public tick (now: number) {
        if (now - this.lastUpdate < this.params.updatePeriod) return;
        this.lastUpdate = now;
        this.fetch();

        // Calcul des différences sur les boutons et déclenchement des events liés
        let buttons = this.getButtons();

        if (buttons.menu != this.prev_btns.menu) this.__buttonState("menu", buttons.menu);
        if (buttons.options != this.prev_btns.options) this.__buttonState("options", buttons.options);

        if (buttons.joystick.L != this.prev_btns.joystick.L) this.__buttonState("L3", buttons.joystick.L);
        if (buttons.joystick.R != this.prev_btns.joystick.R) this.__buttonState("R3", buttons.joystick.R);

        if (buttons.bumper.L != this.prev_btns.bumper.L) this.__buttonState("L2", buttons.bumper.L);
        if (buttons.bumper.R != this.prev_btns.bumper.R) this.__buttonState("R2", buttons.bumper.R);

        if (buttons.dPad.up != this.prev_btns.dPad.up) this.__buttonState("up", buttons.dPad.up);
        if (buttons.dPad.down != this.prev_btns.dPad.down) this.__buttonState("down", buttons.dPad.down);
        if (buttons.dPad.left != this.prev_btns.dPad.left) this.__buttonState("left", buttons.dPad.left);
        if (buttons.dPad.right != this.prev_btns.dPad.right) this.__buttonState("right", buttons.dPad.right);
        
        if (buttons.user.A != this.prev_btns.user.A) this.__buttonState("A", buttons.user.A);
        if (buttons.user.B != this.prev_btns.user.B) this.__buttonState("B", buttons.user.B);
        if (buttons.user.X != this.prev_btns.user.X) this.__buttonState("X", buttons.user.X);
        if (buttons.user.Y != this.prev_btns.user.Y) this.__buttonState("Y", buttons.user.Y);

        this.prev_btns = buttons;

        this.__tick();

    }

    // Events

    private __disconnect () {
        window.removeEventListener("gamepaddisconnected", this.disconnectListener);
        this.dispatchEvent(new Event("disconnect"));
    }

    private __buttonState (button: string, state: bit) {
        let event = `button_${button}_${state && 'down' || 'up'}`;
        this.dispatchEvent(new Event(event));
    }

    private __tick () {
        this.dispatchEvent(new CustomEvent("tick", { detail: this.state }));
    }

    // Getters-Setters
    get dx (): number {
        let dx: number = this.raw?.axes[3] || 0;
        return Math.abs(dx) > this.params.deadZoneR ? ( this.params.xDirection * this.params.positionAccel * dx ) : 0
    }
    get dy (): number {
        let dy: number = this.raw?.axes[2] || 0;
        return Math.abs(dy) > this.params.deadZoneR ? ( this.params.yDirection * this.params.positionAccel * dy ) : 0
    }
    get dalpha (): number {
        let dalpha: number = this.raw?.axes[0] || 0;
        return Math.abs(dalpha) > this.params.deadZoneR ? ( this.params.alphaDirection * this.params.rotationAccel * dalpha ) : 0
    }
    get throttle (): number {
        return this.raw?.buttons[6].pressed ? -this.raw.buttons[6].value : (this.raw?.buttons[7].value || 0);
    }

    get buttons (): GamepadButtons {  
        return this.getButtons();
    }

    get state (): GamepadState {
        return { buttons: this.buttons, dx: this.dx, dy: this.dy, dalpha: this.dalpha, throttle: this.throttle };
    }

}