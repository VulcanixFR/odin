export type callback<T> = (val: T) => void;

export class Store {

    private data: Map<string, string>;
    private callbacks: { cb: callback<any>, key: RegExp, raw_key: string }[] = [];
    private id = "";
    private __enable_save: boolean = false;
    private __has_restored: boolean = false;

    constructor (id?: string) {
        this.data = new Map();
        if (id) {
            this.id = id;
            this.__enable_save = true;
            this.load();
        } 
    }

    // Retourne la valeur d'une clé si existante
    value <T>(key: string): T | undefined {
        if (!this.data.has(key)) return undefined;
        return <T>JSON.parse(<string>this.data.get(key));
    }

    // Assigne une valeur à une clé
    set <T>(key: string, value: T, weak: boolean = false): void {
        if (weak && this.__has_restored) return;
        this.data.set(key, JSON.stringify(value));
        this.__publish<T>(key, value);
        this.save();
    }

    // Supprime une entrée et les callbacks associés
    rm (key: string): void {
        this.data.delete(key);
        this.callbacks = this.callbacks.filter(e => e.raw_key != key);
        this.save();
    }

    /**
     * Attache un callback
     * 
     * @param raw_key bash-like pattern  !! You can't nest curly braces  --  the "path" doesn't matter
     * @param cb callback
     * 
     * @example Valid patterns: "menu.button" ; "link[0]" ; "{menu|link.*}[0]" ; "file.{pdf,txt}"  
     * @example Invalid pattern: "{menu.{a,b}}[0]"
     */
    hook <T> (raw_key: string | undefined, cb: callback<T>): void {
        if (!raw_key) return;
        let temp = raw_key
            .replace(/\./g, "\\.")
            .replace(/\*/g, ".+")
            .replace(/\(([^\)]+)\)/g, "\\($1\\)")
            .replace(/\[([^\]]+)\]/g, "\\[$1\\]")
            .replace(/{([^}]+)}/g, "($1)")
            .replace(/,/g, "|");
        let key = new RegExp(temp);
        this.callbacks.push({ raw_key, key, cb });
    }

    // Détache un callback
    unhook <T> (cb: callback<T>): void {
        this.callbacks = this.callbacks.filter(e => e.cb != cb);
    }

    // Sauvegarde le Store dans le localStorage
    private save (): void {
        if (!this.__enable_save) return;
        let prefix = `__${this.id}__`;
        let liste: string[] = [];
        for (let item of this.data.entries()) {
            localStorage.setItem(`${prefix}${item[0]}`, item[1]);
            liste.push(item[0]);
        }
        localStorage.setItem(prefix, liste.join(";"));
    }

    // Charge le Store depuis le localStorage
    load () {
        let prefix = `__${this.id}__`;
        let raw_liste = localStorage.getItem(prefix);
        // Pas de sauvegarde
        if (raw_liste == null) return;
        this.__has_restored = true;
        let liste = raw_liste.split(";");
        for (let key of liste) {
            let i = localStorage.getItem(prefix + key);
            if (i == null) continue;
            this.data.set(key, i);
        }
    }

    // Efface le Store du localstorage
    clear () {
        if (!this.__enable_save) return;
        let prefix = `__${this.id}__`;
        let raw_liste = localStorage.getItem(prefix);
        // Pas de sauvegarde
        if (raw_liste == null) return;
        this.__has_restored = true;
        let liste = raw_liste.split(";");
        for (let key of liste) {
            localStorage.removeItem(prefix + key);
        }
        localStorage.removeItem(prefix);
    }

    // Retourne la liste des clés contenues par le Store
    get list (): string[] {
        let k: string[] = [];
        for (let key of this.data.keys()) k.push(key);
        return k;
    }

    // Appelle l'ensemble des callbacks concernés par le changement de la valeur contenue en "key"
    private __publish <T>(key: string, value: T): void {
        let toCall = this.callbacks.filter(e => key.match(e.key));
        for (let C of toCall) {
            C.cb(value);
        }
    } 

}