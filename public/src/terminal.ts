import { OdinRelayHTMLClient } from "./RelayClient";
import { Terminal } from "xterm";
import { Server } from "./socket";
import { Store } from "./store";
import { FitAddon } from "xterm-addon-fit";

declare global {
    interface Window {
        term: Terminal;
        store: Store;
        client: Server,
        relay: OdinRelayHTMLClient,
    }
}

const defURL = `${location.protocol == "http" ? "ws" : "wss"}://${location.hostname}`;

window.onload = () => {

    let store = window.store = new Store("terminal");
    store.set("server", defURL, true);

    const term = window.term = new Terminal();
    term.open(<HTMLDivElement>document.querySelector(".terminal"));

    let client = window.client = new Server(store.value("server") || defURL);
    
    let relay = window.relay = new OdinRelayHTMLClient(client, term);
    

    let fitAddon = new FitAddon();
    term.loadAddon(fitAddon);
    fitAddon.fit();

    window.onresize = () => fitAddon.fit();

}
