import { GamepadInput, GamepadState } from "./io/gamepad";
import { MenuButton, MenuButtonDescriptor } from "./menu/fields/button";
import { MenuExpand, MenuExpandDesctiptor } from "./menu/fields/expand";
import { MenuImgbox } from "./menu/fields/imgbox";
import { MenuLink } from "./menu/fields/link";
import { MenuNav } from "./menu/fields/nav";
import { MenuTextbox } from "./menu/fields/textbox";
import { MenuTitle } from "./menu/fields/title";
import { Menu } from "./menu/menu";
import { MenuPage } from "./menu/page";
import { PromptPage } from "./menu/pages/Prompt";
import { TestPage } from "./menu/pages/TestPage";
import { Odin } from "./Odin";
import { descriptionObjet, Plateau } from "./plateau/plateau";
import { Robot } from "./plateau/robtot";
import { Server } from "./socket";
import { Store } from "./store";

declare global {
    interface Window {
        plateau: Plateau;
        server: Server;
        menu: Menu,
        odin: Odin,
        gamepad: GamepadInput[];
        stores: { [key: string]: Store };
        vrac: any;
    }
};

window.onload = () => {

    let html_plateau = <HTMLDivElement>document.querySelector(".plateau");
    let plateau = window.plateau = new Plateau(html_plateau);

    plateau.overlay.mode = "always";
    
    /*
    let server = window.server = new Server(`${location.protocol == "http" ? "ws" : "wss"}://${location.host}`);
    // server.addEventListener("serverLost", () => {
    //     alert("Pas moyen de se connecter à un serveur");
    // }, { once: true });
    server.addEventListener("timeout", () => server.url = `${location.protocol == "http" ? "ws" : "wss"}://${location.hostname}:12042`, { once: true });
    server.addEventListener("identity", () => {
        console.log("Identifié en tant que", server.uuid)
    });
    */

    /**
     * Tests:
     */

    
    window.gamepad = [];
    let noms = [ "Argon", "Brome", "Carbone", "Dubnium", "Europium", "Francium", "Gallium" ];
    let i = 0;

    window.addEventListener("gamepadconnected", e => {
        let robot = new Robot(noms[i % 7]);

        let robotIndex = plateau.ajouteRobot(robot);
        robot.pos = { x: 0, y: 0 };
        robot.user_ctrl = true;
        plateau.focus(robot);

        let gp = new GamepadInput(e.gamepad.index);
        window.gamepad[gp.padIndex] = gp;
        console.log(gp);

        gp.params.xDirection = -1;
        gp.params.yDirection = -1;
        gp.params.alphaDirection = 1;

        gp.params.positionAccel = 5;
        gp.params.rotationAccel = 2;
        
        gp.addEventListener("tick", ((e: CustomEvent<GamepadState>) => {
            let { dx, dy, dalpha, throttle, buttons } = e.detail;
            let focus = plateau.focused == robot;
            let zoomAccel = .005;
            let angle = Math.PI / 512;

            // Zoom plateau
            if (focus) {
                plateau.echelle += zoomAccel * ( -buttons.bumper.L || buttons.bumper.R );
            }

            // Position
            robot.bouge(dx, dy);

            // Rotation
            robot.angle += dalpha * angle;

        }) as EventListener);

        gp.addEventListener("button_X_down", () => {
            if (plateau.focused == robot) {
                plateau.stopFocus()
            } else {
                plateau.focus(robot);
            }
        })

        gp.addEventListener("disconnect", () => {
            robot.remove();
            plateau.stopFocus();
            plateau.overlay.rm(robot);
        })

        // Truc à l'arrache pour test
        gp.addEventListener("button_A_down", () => {
            stores.text.set("gp.ok", true);
        })
        gp.addEventListener("button_A_up", () => {
            stores.text.set("gp.ok", false);
        })
        gp.addEventListener("button_B_down", () => {
            stores.text.set("gp.annul", true);
        })
        gp.addEventListener("button_B_up", () => {
            stores.text.set("gp.annul", false);
        })
        gp.addEventListener("button_options_down", () => {
            stores.text.set("gp.options", true);
        })
        gp.addEventListener("button_options_up", () => {
            stores.text.set("gp.options", false);
        })
        gp.addEventListener("button_down_down", () => {
            stores.text.set("gp.down", true);
        })
        gp.addEventListener("button_up_down", () => {
            stores.text.set("gp.up", true);
        })
        gp.addEventListener("button_menu_down", () => {
            stores.text.set("gp.menu", !stores.text.value("gp.menu"));
        })
        
    });

    tick();
    
    let fileloader = <HTMLInputElement>document.querySelector(".fileloader input[type=file]");
    let btn = <HTMLButtonElement>document.querySelector(".fileloader button")
    btn.addEventListener("click", e => {
        if (!fileloader.files || !fileloader.files[0]) return;
        
        let FR = new FileReader();
        FR.onload = () => playData(FR.result?.toString() || '');
        FR.readAsText(fileloader.files[0]);
    })

    let html_menu = <HTMLDivElement>document.querySelector(".menu");
    let menu = window.menu = new Menu(html_menu);

    let stores: typeof window.stores = window.stores = { };
    stores.text = new Store("text");

    testAvecMenu(stores.text);
    // let odin = window.odin = new Odin();

    stores.text.set("settings.lightmode", !(matchMedia("(prefers-color-scheme: dark)").matches), true);
    mode_clair = stores.text.value("settings.lightmode") || false;
    document.body.classList.toggle("light", mode_clair)
    document.body.classList.toggle("dark", !mode_clair)

    stores.text.hook<boolean>("settings.acrylic", b => menu.acrylic = b);

    stores.text.hook<boolean>("gp.menu", v => {
        window.menu.lock = v;
        window.menu.popupActive = v;
    })

    stores.text.set("settings.acrylic", true, true);
    stores.text.set("gp.menu", true);
    window.menu.acrylic = stores.text.value("settings.acrylic") || false;

}

let mode_clair = false;
let keys = {
    esc: false,
    enter: false,
    up: false,
    down: false,
    back: false,
};

document.onkeyup = e => {
    if (e.key == "Escape") keys.esc = false;

    else if (e.key == "Enter") {
        keys.enter = false;
        window.stores.text.set("gp.ok", false);
    }
    
    else if (e.key == "ArrowUp") {
        keys.up = false;
        window.stores.text.set("gp.up", false);
    } 
    
    else if (e.key == "ArrowDown") {
        keys.down = false;
        window.stores.text.set("gp.down", false);
    } 
    
    else if (e.key == "Backspace") {
        keys.back = false;
        window.stores.text.set("gp.annul", false);
    }


    
}
document.onkeydown = e => {
    if (e.key == "Escape") {
        if (keys.esc) return;
        keys.esc = true;
        window.stores.text.set("gp.menu", !window.stores.text.value("gp.menu"));
    } 
    
    else if (e.key == "Enter") {
        if (keys.enter) return;
        keys.enter = true;
        window.stores.text.set("gp.ok", true);
    } 
    
    else if (e.key == "ArrowUp") {
        if (keys.up) return;
        keys.up = true;
        window.stores.text.set("gp.up", true);
    } 
    
    else if (e.key == "ArrowDown") {
        if (keys.down) return;
        keys.down = true;
        window.stores.text.set("gp.down", true);
    } 
    
    else if (e.key == "Backspace") {
        if (keys.back) return;
        keys.back = true;
        window.stores.text.set("gp.annul", true);
    } 
    
    else if (e.key == "o") {
        window.stores.text.set("gp.options", true);
    } 
    
    console.log(e.key);         
}

function tick () {

    let now = Date.now();
    window.gamepad.forEach(e => e.tick(now));

    window.requestAnimationFrame(() => tick());

}

function *getData (raw: string): Generator<{date: Date, objets: descriptionObjet[]}, void> {
    let lns = raw.split("\n");
    for (let ln of lns) {
        let data = ln.split("/");
        let rawDate = data.shift();
        if (rawDate == "ERR" || rawDate == "LOG" || !rawDate) {
            console.log ('Pas de données >\n', data.join(': '));
            continue;
        }

        let date = new Date(parseInt(rawDate));
        let objets: descriptionObjet[] = data.map(e => {
            let [ type, uid, opt, x, y, alpha ] = e.split(";");
            return <descriptionObjet>({
                type, uid: parseInt(uid), opt, 
                x: parseFloat(x), y: parseFloat(y), alpha: parseFloat(alpha)
            })
        });

        yield { date, objets };
    }

    alert("Fini !")
}

function playData (rawData: string) {
    let data = getData(rawData);

    let it: any = setInterval(() => {
        let d = data.next();
        if (d.done) return clearInterval(it);
        window.plateau.chargeDescriptions(d.value.objets);
    }, 1000 / 10);
}

const lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, omnis praesentium. Neque ipsam optio sequi fugiat cumque facere delectus id porro qui. At sequi officiis vel velit vero maxime omnis?";

function testAvecMenu (textes: Store) {

    let popup = document.querySelector(".popup .content");
    if (!popup) throw "Pas de menu"; 

    let pages = popup.querySelector(".pages");
    if (!pages) throw "Pas de pages";

    let ctrls = document.querySelector(".controls");
    if (!ctrls) throw 'Pas de controls';

    pages.remove();
    ctrls.remove();
    
    let nav = new MenuNav("__menu_nav", [ "Odin" ], "menu.nav", textes);
    popup.appendChild(nav.self);    
    popup.appendChild(pages);    
    popup.appendChild(ctrls);    

    // Boutons du bas
    let btnOK = new MenuButton("_menu_ctrl_ok", { text: "OK", img: 'lib/controllerIcons/Xbox Series/XboxSeriesX_A.png' }, "menu.ctrl.ok", textes);
    let btnAnnul = new MenuButton("_menu_ctrl_annul", { text: "Annuler", img: 'lib/controllerIcons/Xbox Series/XboxSeriesX_B.png' }, "menu.ctrl.annul", textes);
    let slots = [  
        <HTMLSpanElement>ctrls.querySelector(".slot-left"),
        <HTMLSpanElement>ctrls.querySelector(".slot-right"),
    ];
    slots[0].appendChild(btnAnnul.self);
    slots[1].appendChild(btnOK.self);

    let currPage: MenuPage;

    let page = new TestPage(textes);
    pages.appendChild(page.elem);
    page.loaded = true;

    currPage = page;

    btnAnnul.onclick = () => currPage.leave();

    console.dir(window.vrac = { btnAnnul, btnOK, page /*, deux, img */})

    // Mode clair/sombre
    textes.hook<boolean>("settings.lightmode", (d) => {
        document.body.classList.toggle("light", mode_clair = d);
        document.body.classList.toggle("dark", !mode_clair)
    })

    textes.hook<boolean>("gp.ok", v => {
        btnOK.active = v;
        if(!v) currPage.click();
    });

    textes.hook<boolean>("gp.annul", v => {
        btnAnnul.active = v;
    });

    textes.hook<boolean>("gp.options", v => {
        if (!currPage.interactive) currPage.enter();
    });

    textes.hook<boolean>("gp.down", v => {
        if (v) currPage.next();
    })

    textes.hook<boolean>("gp.up", v => {
        if (v) currPage.prev();
    })


    let prompt: PromptPage;
    page.onedit = data => {

        let cb = (str: string) => {
            prompt.elem.remove();
            pages?.appendChild(page.elem);
            prompt.loaded = false;
            page.loaded = true;
            currPage = page;
            data.cb(str);
        } 

        prompt = new PromptPage(textes, `Modification de ${data.item.key}`, data.item.value, data.item.key, cb);
        prompt.onabort = () => cb(data.item.value);

        page.elem.remove();
        pages?.appendChild(prompt.elem);
        page.loaded = false;
        prompt.loaded = true;
        currPage = prompt;

    }

}
