import { RelayMessage } from "./RelayClient";
import { v4 } from "uuid";
export class Server extends EventTarget {

    private socket: WebSocket;
    private _uuid: string;
    private _srvUid: string;
    private connexion: boolean = false;

    public timeout = 2000;
    public tentatives = 0;

    constructor (private _url: string) {

        super();

        this.socket = new WebSocket(_url);
        this._uuid = "";
        this._srvUid = "";
        
        this.initSocket();

    }

    private initSocket(force = false) {

        if (force) this.socket = new WebSocket(this._url);
        this._uuid = "";

        this.socket.onopen = () => {
            console.log("Connecté au serveur " + this._url);
            this.connexion = true;
            this.tentatives = 0;
            this.sayHello();
        }

        this.socket.onmessage = msg => this.handleMsg(msg);

        this.socket.onerror = e => {
            console.error("Erreur connexion:", e);
        }

        this.socket.onclose = e => {
            this.connexion = false;
            console.error("Connexion fermée:", e);
            this.__connClosed()
            
            if (++this.tentatives > 3) {
                console.log("Serveur mort crispé.")
                this.__serverLost();
                return;
            };

            console.log("Tentative de reconnexion ...");

            setTimeout(() => {
                this.socket = new WebSocket(this._url);
                this.initSocket();
            }, this.timeout);
        }
        
        setTimeout(() => {
            if (!this.connexion) {
                console.log("Connexion: Timeout");
                this.__timeout();
            }
        }, this.timeout);

    }

    private handleMsg (msg: MessageEvent) {
        let payload: OdinPayload<any> = JSON.parse(msg.data.toString());

        // Not listening itself
        if (payload.__sender.name == this.uuid) return;

        if (payload.__sender.type == "relay") {
            this.__relay(payload);
            return;
        }

        let data = (<OdinPayload<SrvMsgs>>payload).__data;

        switch (data.type) {
            case "hello":
                this._uuid = data.uuid;
                this._srvUid = data.srvUid;
                this.__identity();
                break;
            default:
                console.log(payload);
        }

    }

    send (payload: OdinPayload<any>): boolean {
        if (!this.connexion) return false;
        this.socket.send(JSON.stringify(payload));
        return true;
    }

    private sayHello () {
        let payload: OdinPayload<ClientHello> = this.__emptyPayload(v4());
        payload.__data = { type: "hello" };
        this.send(payload);
    }

    /* Events */
    private __connClosed () {
        this.dispatchEvent(new Event("connClosed"));
    }
    private __serverLost () {
        this.dispatchEvent(new Event("serverLost"));
    }
    private __timeout () {
        this.dispatchEvent(new Event("timeout"));
    }
    private __identity () {
        this.dispatchEvent(new Event("identity"));
        this.__connected();
    }
    private __connected () {
        this.dispatchEvent(new Event("connected"));
    }
    private __relay (data: OdinPayload<RelayMessage>) {
        this.dispatchEvent(new CustomEvent("relay", { detail: data }));
    }

    /* Getters-Setters */

    get url () { return this._url }
    set url (url) {
        this._url = url;
        if (this.connexion) {
            this.socket.close();
        } else {
            this.tentatives = 0;
            this.initSocket(true);
        }
    }

    get uuid () {
        return this._uuid;
    }

    private __emptyPayload (uid: string): OdinPayload<any> {
        return {
            __message: { for: this._srvUid, uid },
            __sender: { name: this._uuid, type: "client", via: [] },
            __data: undefined,
        }
    }

}