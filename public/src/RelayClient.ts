import { Server } from "./socket";
import { v4 } from "uuid";
import { Terminal } from "xterm";

export class OdinRelayHTMLClient {

    private __server: string = "";
    private __ready: boolean = false;
    private __srv_list: string[] = [];

    private __requests: { [key: string]: (response: RelayResponse<any>) => void } = {};
    private __buffer: string = "";
    private __input_mode: string = "";

    constructor (private client: Server, private term: Terminal) {

        this.log("Init");

        client.addEventListener("relay", ((event: CustomEvent) => {
            let data = event.detail;
            if (!data) return;
            this.handle(data);
        }) as EventListener);

        client.addEventListener("identity", () => {
            this.__sendAttach();
            this.__sendDiscover();
            this.log("Attaching to Relay")
        })

        client.addEventListener("connClosed", () => {
            this.__ready = false;
            this.__buffer = "";
            this.__srv_list = [];
            this.__server = "";
            this.__input_mode = "";
            this.log("Serveur relais mort", true);
        })

        term.onData(d => {
            this.__handleRawTerm(d);
        })

        term.onResize (evt => {
            this.__sendResize(evt.cols, evt.rows);
            console.log(evt.cols, evt.rows);
        })

    }

    handle (payload: OdinPayload<RelayMessage>) {

        console.log("<|", payload.__sender.name, "|", payload.__data.event);

        switch (payload.__data.event) {

            case "connect":
                this.__handleConnect(<OdinPayload<RelayClientConnect>>payload);        
                break;

            case "disconnect":
                this.__handleDisconnect(<OdinPayload<RelayClientDisconnect>>payload);
                break;

            case "discover":
                this.__handleDiscover(<OdinPayload<RelayDicover>>payload);
                break;

            case "response":
                this.__handleResponse(<OdinPayload<RelayResponse<any>>>payload);
                break;

            case "request":
                this.__handleRequest(<OdinPayload<RelayRequest>>payload);
                break;

            case "rawTerm":
                this.__handleRcvTerm(<OdinPayload<RelayRawTerminal>>payload);
                break;

            default:
                console.warn(payload);

        }

    }

    log (text: string, err?: true) {
        this.term.write(`\x1b[${err ? 31 : 36}m[Relay] ${text}\x1b[0m\r\n`);
    }

    private __handleConnect (payload: OdinPayload<RelayClientConnect>) {
        if (payload.__data.uuid == this.client.uuid) return;
        this.__sendDiscover(payload.__data.uuid);
    }

    private __handleDiscover (payload: OdinPayload<RelayDicover>) {
        if (this.__srv_list.indexOf(payload.__data.uuid) != -1) return;
        this.__srv_list.push(payload.__data.uuid);
        this.log(`New server appeared: ${payload.__data.uuid} [${ payload.__data.hostname || "N/A" }]`);
    }

    private __handleDisconnect (payload: OdinPayload<RelayClientDisconnect>) {
        this.__srv_list = this.__srv_list.filter(e => e != payload.__data.uuid);
        this.log(`Server died: ${payload.__data.uuid}`);
        if (payload.__data.uuid == this.__server) {
            this.log("You are now disconnected", true);
            this.__ready = false;
        }
    }

    private __handleRequest (payload: OdinPayload<RelayRequest>) {
        // Rien à faire
    }

    private __handleRcvTerm (payload: OdinPayload<RelayRawTerminal>) {
        this.term.write(payload.__data.data);
    }

    private __handleResponse (payload: OdinPayload<RelayResponse<any>>) {
        if (!(payload.__message.uid in this.__requests)) return;
        this.__requests[payload.__message.uid](payload.__data);
    }

    private __sendDiscover (uuid?: string) {
        let payload: OdinPayload<RelayRequest> = this.__emptyPayload(v4());
        payload.__data = {
            req: "discover", args: [], event: "request"
        }
        payload.__message.for = uuid || '*';
        this.client.send(payload);
    }

    private __sendAttach () {
        let payload: OdinPayload<ClientAttachRelay> = this.__emptyPayload(v4());
        payload.__data = { type: "relay.attach" };
        payload.__sender.type = "client";
        this.client.send(payload);
    }

    private __sendRawTerm (data: string) {
        if (!this.__ready) return;
        let payload: OdinPayload<RelayRawTerminal> = this.__emptyPayload(v4());
        payload.__data = { data, event: "rawTerm" } 
        this.client.send(payload);
    }

    private __sendResize (col: number, row: number) {
        if (!this.__ready) return;
        let uid = v4();
        let payload: OdinPayload<RelayRequest> = this.__emptyPayload(uid);
        payload.__data = { event: "request", req: "resize", args: [ col.toString(), row.toString() ] }
        this.__requests[uid] = (res: RelayResponse<string>) => {
            if (res.error) this.log(res.value, true);
        }        
        this.client.send(payload);
    }

    private __requestTerm () {
        if (this.__ready) return;

        let uid = v4();
        let payload: OdinPayload<RelayRequest> = this.__emptyPayload(uid);
        payload.__data = { event: "request", req: "terminal", args: [ ] }
        this.__requests[uid] = (res: RelayResponse<string>) => {
            this.log(res.value, res.error);
            this.__ready = !res.error;            
            this.__sendResize(this.term.cols, this.term.rows);
        }        
        this.client.send(payload);

    }

    private __listServers () {
        this.term.write(`\r\n\x1b[32mServer list:\x1b[0m\r\n`);
        for (let i in this.__srv_list) {
            this.term.write(`\x1b[33m ${i} \x1b[35m - ${this.__srv_list[i]}\x1b[0m\r\n`);
        }
        this.term.write("\r\n");
    }

    private __handleRawTerm (data: string) {
        console.log(data);
        
        if (this.__ready) {
            this.__sendRawTerm(data);
            return
        }

        if (data == "\r") {
            switch (this.__input_mode) {
                case "srv_nb":
                    this.__input_mode = "";
                    this.term.write("\n\r");
                    let nb = parseInt(this.__buffer);
                    if (Number.isNaN(nb) || nb >= this.__srv_list.length) {
                        this.log("Invalid input", true);
                    } else {
                        this.__server = this.__srv_list[nb];
                        this.log("Selected server " + nb);
                    }
                    break;
                default:
                    // Rien
            }
        }

        if (this.__input_mode != "") {
            this.__buffer += data;
            this.term.write(data);
            return;
        } 

        switch (data) {
            case "l":
                this.__listServers();
                break;
            case "c":
                this.log('Server n° ?');
                this.term.write("> ");
                this.__input_mode = "srv_nb";
                break;
            case "s":
                this.log("Selected server: " + this.__server);
                break;
            case "q":
                this.__requestTerm();
                break;
        }
        
    }

    private __handleResize (col: number, row: number) {
        if (this.__ready) this.__sendResize(col, row);
    }

    private __emptyPayload (uid: string): OdinPayload<any> {

        return {
            __data: undefined,
            __message: { for: this.__server, uid },
            __sender: { name: this.client.uuid, type: "relay", via: [] }
        }

    }

}

/* Volontairement re-défini pour partager ce fichier en dehors d'ici */
export type RelayMessage = 
      RelayClientConnect 
    | RelayClientDisconnect
    | RelayResponse<any>
    | RelayRequest
    | RelayRawTerminal
    | RelayDicover;

type RelayResponse<T> = {
    event: 'response', error?: true, value: T
}

type RelayRequest = {
    event: 'request', req: string, args: string[]
}

type RelayRawTerminal = {
    event: 'rawTerm', data: string
}

type RelayClientConnect = {
    event: "connect", uuid: string
}

type RelayClientDisconnect = {
    event: "disconnect", uuid: string
}

type RelayDicover = {
    event: "discover", uuid: string, hostname?: string
}
