import { Store } from "../../store";
import { MenuField } from "../fields"

export class MenuImgbox extends MenuField<string> {

    private __img = document.createElement("img");

    constructor (id: string, value: string, hook?: string, store?: Store) {
        super('imgbox', id, value, hook, store);
        this.elem.className = "imgbox";
        this.__img.onload = () => {
            this.elem.innerHTML = "";
            this.elem.appendChild(this.__img);
        }
        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        this.__img.remove();
        this.elem.innerHTML = '<span class="loader"></span>';
        this.__img.src = this.value;
    }

}