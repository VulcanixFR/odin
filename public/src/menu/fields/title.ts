import { MenuField } from "../fields"

const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@/?%$&";
const randomChar = () => chars[~~(Math.random() * chars.length)];
export class MenuTitle extends MenuField<string> {

    private __interval: number = -1;

    constructor (id: string, value: string) {
        let elem = document.createElement("h1");
        super('title', id, value, undefined, undefined, elem);
        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        clearInterval(this.__interval);
        let target = this.value;
        let iteration = 0;
        let word = target.split("");
        let len = word.length;

        this.__interval = window.setInterval(() => {
            for (let i = 0; i < len; i++) {
                word[i] = i < iteration ? target[i] : randomChar();
            }
            this.elem.innerText = word.join("");
            if (++iteration > target.length) clearInterval(this.__interval);
        }, 20);

    }

}