import { Store } from "../../store";
import { MenuField } from "../fields"

export class MenuTextbox extends MenuField<string> {

    constructor (id: string, value: string, hook?: string, store?: Store) {
        super('textbox', id, value, hook, store);
        this.elem.className = "infocard";
        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        this.elem.innerText = this.value;
    }

}