import { Store, callback } from "../../store";
import { MenuField } from "../fields"

export type MenuExpandItemDesctiptor = 
      { type: 'field',    key: string,  value: string,  data_type?: string                } 
    | { type: 'checkbox', key: string,  text: string,   value: boolean                    } 
    | { type: 'radio',    key: string,  text: string,   value: boolean,     index: number } 

export type MenuExpandDesctiptor = { 
    icon: string; text: string; fa_type?: string, value?: string;
    items: MenuExpandItemDesctiptor[];  
};

export type MenuExpandItem = 
      { type: 'field', cb: callback<string>, elem: HTMLLIElement } 
    | { type: 'checkbox', cb: callback<boolean>, elem: HTMLLIElement, input: HTMLInputElement }
    | { type: 'radio', cb: callback<number>, elem: HTMLLIElement, input: HTMLInputElement };

export class MenuExpand extends MenuField<MenuExpandDesctiptor> {

    private __hovered: number = -1;
    private __items: MenuExpandItem[] = [];
    private __title: HTMLLabelElement;
    private __ul: HTMLUListElement;
    private __inhibit_html_update: boolean = false;

    public onedit: callback<{ cb: callback<string>, item: MenuExpandItemDesctiptor & { type: "field" } }> = () => {};

    constructor (id: string, value: MenuExpandDesctiptor, group: string, hook?: string, store?: Store) {
        super('expand', id, value, hook, store, undefined, true);
        this.elem.className = "expand";
        this.elem.onclick = () => {
            if (this.disabled) return;
            this.onclick(this);
        };
        this.elem.innerHTML = ExpandHTML(this.value, this.id, group);
        this.__title = <HTMLLabelElement>this.elem.querySelector("label.titre");
        this.__ul = <HTMLUListElement>this.elem.querySelector("ul");
        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        if (this.__inhibit_html_update) return;
        this.__title.innerHTML = LabelHTML(this.value);
        this.__items.forEach(e => { 
            if (this.store) {
                this.store.unhook<any>(e.cb);
            }
            e.elem.remove();
        });
        let inputHandler: callback<number> = n => this.__handle_input(n);
        this.__items = this.value.items.map((e, i) => {
            let item = ExpandItem(e, this.id, i, inputHandler, e.key, this.store);
            this.__ul.appendChild(item.elem);
            return item;
        });
        this.elem.style.setProperty("--nb-elem", this.__items.length.toString());
        this.__update_hover();
        this.elem.classList.toggle("disabled", this.disabled);
    }

    private __update_hover () {
        this.__items.forEach((e, i) => {
            e.elem.querySelector("span")?.classList.toggle("hover", i == this.__hovered);
        });
    }

    private __handle_input (n: number) {
        let item = this.value.items[n];
        let _item = this.__items[n];
        let val: typeof this.value;

        if (!item || !_item) return;
        if (item.type != _item.type) return;

        switch (_item.type) {

            case "field":
                let field = <MenuExpandItemDesctiptor & { type: "field" }>item;
                let cb: callback<string> = (neo) => {
                    // Update descriptors
                    let val = this.value;
                    (<typeof item>val.items[n]).value = neo;
                    this.value = val;
                    // Update linked var
                    this.store?.set<string>(item.key, neo);
                };
                this.onedit({ cb, item: field });
                break;

            case 'checkbox':
                let checkbox = <MenuExpandItemDesctiptor & {type: "checkbox"}>item;
                this.__inhibit_html_update = true;
                val = this.value;
                (<typeof item>val.items[n]).value = _item.input.checked;
                this.value = val;
                this.store?.set<boolean>(item.key, _item.input.checked);
                this.__inhibit_html_update = false;
                break;

            case 'radio':
                let radio = <MenuExpandItemDesctiptor & {type: "radio"}>item;
                this.__inhibit_html_update = true;
                val = this.value;
                val.items.filter(e => e.type == "radio").forEach(e => e.value = false);
                (<typeof item>val.items[n]).value = _item.input.checked;
                this.value = val;
                this.store?.set<number>(item.key, radio.index);
                this.__inhibit_html_update = false;
                break;

        }
    }

    // Selectionne l'élément suivant
    next () {
        if (this.__hovered == -1) return;
        this.__hovered++;
        if (this.__hovered == this.__items.length) 
            this.__hovered = 0;
        this.__update_hover();
    }

    // Selectionne l'élément précédent
    prev () {
        if (this.__hovered == -1) return;
        this.__hovered--;
        if (this.__hovered < 0) 
            this.__hovered = this.__items.length - 1;
        this.__update_hover();
    }

    // Active le mode Hover
    enter () {
        this.__hovered = 0;
        this.active = true;
        this.__update_hover();
    }

    // Arrête le mode hover
    leave () {
        this.__hovered = -1;
        this.active = false;
        this.__update_hover();
    }

    // Active l'élément survolé
    click () {
        if (this.__hovered == -1) return;
        let item = this.value.items[this.__hovered];
        switch (item.type) {
            case "field":
                (<() => void>this.__items[this.__hovered].elem.onclick)();
                break;
            case "checkbox":
            case "radio":
                let input = <HTMLInputElement>this.__items[this.__hovered].elem.querySelector("input");
                input.checked = !input.checked;
                (<() => void>input.onchange)();
                break;
        }
    }

}

function ExpandHTML (desc: MenuExpandDesctiptor, id: string, group: string): string {
    return `
    <input class="__menu_select" type="checkbox" name="__menu_${group}" id="__menu_${id}">
    <label class="titre" for="__menu_${id}"></label>
    <ul class="expand_list"></ul>`
}

function LabelHTML (desc: MenuExpandDesctiptor): string {
    return `
<div class="selector">
    <i class="fa-${desc.fa_type || 'solid'} fa-${desc.icon}"></i>
</div>
<span class="text">${desc.text}</span>                                    
<span class="value">${desc.value || '' }</span>
<i class="fa-solid fa-plus"></i>
<i class="fa-solid fa-minus"></i>`;
}

function ExpandItem (desc: MenuExpandItemDesctiptor, id: string, n: number, cb: callback<number>, key?: string, store?: Store): MenuExpandItem {
    let li = document.createElement("li");
    let field_hook: (val: string) => void = () => {};
    let box_hook: (val: boolean) => void = () => {};
    let radio_hook: (val: number) => void = () => {};
    li.className = "expand_list_elem";

    switch (desc.type) {
        case 'field':
            li.innerHTML = ExpandFieldHTML(desc, id, n);
            if (store && key) {
                field_hook = val => {
                    (<HTMLSpanElement>li.querySelector(".value")).innerText = val;
                };    
                store.hook<string>(key, field_hook);
            }
            li.onclick = () => cb(n);
            return { elem: li, cb: field_hook, type: "field" };
        
        case "checkbox":
            li.innerHTML = ExpandBoxHTML(desc, id, n);
            let box = <HTMLInputElement>li.querySelector('input');
            if (store && key) {
                box_hook = val => {
                    box.checked = val;
                }
                store.hook<boolean>(key, box_hook);
                box.onchange = () => store.set(key, box.checked);
            }
            box.onchange = () => cb(n);
            return { elem: li, cb: box_hook, input: box, type: desc.type };

        case "radio":
            li.innerHTML = ExpandBoxHTML(desc, id, n);
            let r = <HTMLInputElement>li.querySelector('input');
            r.dataset.nb = desc.index.toString();
            if (store && key) {
                radio_hook = val => {
                    r.checked = val == desc.index;
                }
                store.hook<number>(key, radio_hook);
            }
            r.onchange = () => cb(n);
            return { elem: li, cb: radio_hook, input: r, type: desc.type };
    
    }
    
}

function ExpandFieldHTML (desc: MenuExpandItemDesctiptor & { type: "field" }, id: string, n: number): string {
    return `
<span class="elist field">
    <span class="key">${desc.key}</span>
    <span class="value">${desc.value}</span>
    <span class="fa-solid fa-arrow-left"></span>
</span>`;
}

function ExpandBoxHTML (desc: MenuExpandItemDesctiptor & { type: "checkbox" | 'radio' }, id: string, n: number): string {
    return `
<span class="elist ${desc.type}">
    <input type="${desc.type}" name="__menu_${id}_choice" id="__menu_${id}_choice_${n}" ${ desc.value && 'checked' || '' }>
    <label for="__menu_${id}_choice_${n}">
        <span class="switch"></span>
        <span class="text">${desc.text}</span>
    </label>
    <span class="fa-solid fa-arrow-left"></span>
</span>
`;
}