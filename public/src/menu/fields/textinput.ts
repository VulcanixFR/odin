import { callback, Store } from "../../store";
import { MenuField } from "../fields"

export class MenuTextInput extends MenuField<string> {

    private __input: HTMLInputElement = document.createElement("input");

    constructor (id: string, value: string, placeholder?: string, hook?: string, store?: Store, autofocus?: true) {
        super('textinput', id, value, hook, store);
        this.elem.className = "textinput";
        this.elem.append(this.__input);

        this.__input.placeholder = placeholder || "";

        this.__input.onkeyup = () => {
            console.log(">", this.__input.value)
            this.value = this.__input.value;
        }

        this.__input.value = this.value;
        this.__input.autofocus = autofocus || false;

        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        this.__input.value = this.value;
        this.__input.disabled = this.disabled;
    }

    focus () {
        this.__input.focus()
    }

}