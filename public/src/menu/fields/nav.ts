import { Store } from "../../store";
import { MenuField } from "../fields"

export type MenuNavDescriptor = string[];

export class MenuNav extends MenuField<MenuNavDescriptor> {

    private path: string[] = [];
    private __spans: HTMLSpanElement[] = [];
    private __updating: boolean = false;

    constructor (id: string, value: MenuNavDescriptor, hook?: string, store?: Store) {
        
        super('nav', id, value, store && hook, store);
        this.elem.className = "nav";

        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        if (this.__updating) return;
        this.__updating = true;
        
        let newPath = this.value;
        let i;

        for (i = 0; i < newPath.length; i++) {
            if (!this.path[i] || this.path[i] != newPath[i]) break;
        }

        if (i == newPath.length) {
            // On doit virer tout ce qui est après cette longueur
            this.__remove(this.path.length - 1, newPath.length);
        } else {
            // On doit modifier les spans à partir d'ici
            this.__add(i, newPath);
        }
        
        this.path = this.value;

    }

    private __remove (i: number, end: number) {
        if (i < end) { 
            this.__updating = false;
            return; 
        }
        let span = this.__spans.pop();
        span?.classList.toggle("rm", true);
        setTimeout(() => {
            this.__remove(i - 1, end);
        }, 100);
        setTimeout(() => {
            span?.remove();
        }, 400);
    }

    private __add (i: number, data: string[]) {
        if (i >= data.length) {
            this.__updating = false;
            return;
        }
        if (this.__spans[i]) {
            this.__spans[i].innerText = data[i];
        } else {

            let span = document.createElement("span");
            span.className = "sub";
            span.innerText = data[i];
            this.__spans.push(span);
            this.elem.appendChild(span);
            span.onclick = () => this.__nav(i);

            setTimeout(() => {
                this.__add(i + 1, data);
            }, 100);

        }
    }

    private __nav (i: number) {
        let oldLength = this.path.length;
        while (this.path.length > i + 1) {
            this.path.pop();
        }
        this.__remove(oldLength, this.path.length + 1);
        this.value = this.path;
    }

}