import { Store } from "../../store";
import { MenuField } from "../fields"

export type MenuLinkDesctiptor = { icon: string; text: string; fa_type?: string, href?: string };

export class MenuLink extends MenuField<MenuLinkDesctiptor> {

    constructor (id: string, value: MenuLinkDesctiptor, hook?: string, store?: Store) {
        super('link', id, value, hook, store);
        this.elem.className = "link";
        this.elem.onclick = () => {
            if (this.disabled) return;
            if (this.value.href) {
                window.open(this.value.href, "__blank");
            } else this.onclick(this);
        };
        this.__ready = true;
        this.__update_html();
    }

    protected __update_html () {
        this.elem.innerHTML = MenuLinkHTML(this.value);
        this.elem.classList.toggle("disabled", this.disabled);
    }

}

function MenuLinkHTML (desc: MenuLinkDesctiptor) {
    return `
<div class="selector">
    <i class="fa-${desc.fa_type || 'solid'} fa-${desc.icon}"></i>
</div>
<span class="text">${desc.text}</span>
<i class="fa-solid fa-arrow-right"></i>`;
}