import { Store } from "../../store";
import { MenuField } from "../fields"

export type MenuButtonDescriptor = { text: string, img: string };

export class MenuButton extends MenuField<MenuButtonDescriptor> {

    private __text: HTMLSpanElement;
    private __img: HTMLImageElement;

    constructor (id: string, value: MenuButtonDescriptor, hook?: string, store?: Store) {
        
        let elem = document.createElement("button");
        elem.innerHTML = __menuDefaultButtonHTML(value.text, value.img);

        super('button', id, value, store && hook, store, elem);

        this.__text = <HTMLSpanElement>elem.querySelector(".text");
        this.__img = <HTMLImageElement>elem.querySelector("img");

        this.__update_html();
        this.__ready = true;
    }

    protected __update_html () {
        this.__text.innerText = this.value.text;
        this.__img.src = this.value.img;
        this.__img.classList.toggle("none", this.value.img == "");
        (<HTMLButtonElement>this.elem).disabled = this.disabled;
    }

    set img (img: string) {
        this.value = { text: this.value.text, img };
    }
    set text (text: string) {
        this.value = { text, img: this.value.img };
    }

    get img () { return this.value.img };
    get text () { return this.value.text };

}

function __menuDefaultButtonHTML (text: string, img: string) {
    return `
    <span class="btn-cont">
        <span class="icon"><img src="${img}" alt="${img}"></span>
        <span class="text">${text}</span>
    </span>
    `;
}