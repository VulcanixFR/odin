import { Store } from "../../store";
import { MenuPage } from "../page";

export class TestPage extends MenuPage {

    constructor (store: Store) {
        super("Page de test", "menu_test_page", store);

        this.text("Ceci est une page de test");
        this.expand({  
            icon: "gears",
            text: "Paramètres",
            items: [
                { type: "checkbox", key: "settings.lightmode", text: "Mode clair", value: false },
                { type: "checkbox", key: "settings.acrylic", text: "Mode Acrylique", value: true },
                { type: "field", key: "vrac.gameData", value: "{}", data_type: "text" }
            ]
        }, "menu.test_page.expand[0]");

        this.link({ icon: "globe", text: "Globe" });
        this.link({ icon: "apple", text: "Pomme", fa_type: "brands", href: "terminal.html" });
        this.link({ icon: "car", text: "Voiture" });

        this.textInput(undefined, "Valeur", "vrac.gameData");

    }

}