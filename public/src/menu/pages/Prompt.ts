import { callback, Store } from "../../store";
import { MenuTextInput } from "../fields/textinput";
import { MenuPage } from "../page";

export class PromptPage extends MenuPage {

    private __input: MenuTextInput;

    constructor (store: Store, titre: string, valeur: string, placeholder: string, private cb: callback<string>, type?: string) {
        super("Page de test", "menu_prompt_page", store);

        this.text(titre);
        this.__input = this.textInput(valeur, placeholder, undefined, true);

    }

    click(): void {
        
        this.cb(this.__input.value);

    }

}