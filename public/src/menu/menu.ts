import { Odin } from "../Odin";

export class Menu {

    private status: { 
        lock: boolean,
        popup: boolean,
        widgets: boolean;
        acrylic: boolean;
    };

    private html: {  
        elem: HTMLDivElement,
        popup: HTMLDivElement,
        widgetContainer: HTMLDivElement,
    };

    constructor (elem: HTMLDivElement) {

        this.status = { 
            lock: false, popup: true, widgets: true,
            acrylic: false,
        };

        this.html = {
            elem,
            popup: <HTMLDivElement>elem.querySelector(".popup"),
            widgetContainer: <HTMLDivElement>elem.querySelector(".widgets"),
        }

        this.updateDisplay();

    }

    private updateDisplay () {

        this.html.elem.classList.toggle("lock", this.status.lock);
        this.html.popup.classList.toggle("hidden", !this.status.popup);
        this.html.widgetContainer.classList.toggle("active", this.status.widgets);

        this.html.popup.classList.toggle("acrylic", this.status.acrylic);

    }

    get lock () { return this.status.lock }
    set lock (l) {
        this.status.lock = l;
        this.updateDisplay();
    }

    get popupActive () { return this.status.popup }
    set popupActive (p) {
        this.status.popup = p;
        this.updateDisplay();
    }

    get widgetsActive () { return this.status.widgets }
    set widgetsActive (w) {
        this.status.widgets = w;
        this.updateDisplay();
    }

    get acrylic () { return this.status.acrylic }
    set acrylic (a) {  
        this.status.acrylic = a;
        this.updateDisplay();
    }

}