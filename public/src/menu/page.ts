import { callback, Store } from "../store";
import { MenuButton, MenuButtonDescriptor } from "./fields/button";
import { MenuExpand, MenuExpandDesctiptor } from "./fields/expand";
import { MenuImgbox } from "./fields/imgbox";
import { MenuLink, MenuLinkDesctiptor } from "./fields/link";
import { MenuNav, MenuNavDescriptor } from "./fields/nav";
import { MenuTextbox } from "./fields/textbox";
import { MenuTextInput } from "./fields/textinput";
import { MenuTitle } from "./fields/title";

// https://degaucheoudedroite.delemazure.fr/?faire%20un%20saumon

export type MenuItems = 
    MenuButton | MenuExpand | MenuImgbox | MenuLink | MenuNav | MenuTextbox | MenuTitle | MenuTextInput;

export class MenuPage {

    private __items: MenuItems[] = [];
    private __interactive: MenuItems[] = [];

    private __status: {
        title: string; id: string;
        interactive: { select: number; expand: MenuExpand | undefined; enable: boolean; };

    }
    private __cpt = 0;
    
    readonly elem = document.createElement("div");

    public onTitleChange: callback<string> = () => {};

    public onedit: typeof MenuExpand.prototype.onedit = () => {};
    public onabort: () => void = () => {};

    public loaded: boolean = false;

    constructor (title: string, id: string, private store: Store) {
        this.__status = {
            title, id,
            interactive: {
                enable: false,
                select: 0,
                expand: undefined
            }
        };
        this.elem.className = "page";
        this.elem.id = id;
    }

    text (text: string, hook?: string): MenuTextbox {
        let box = new MenuTextbox(this.nextId, text, hook, this.store);
        this.__items.push(box);
        this.elem.appendChild(box.self);
        return box;
    }

    link (link: MenuLinkDesctiptor, hook?: string): MenuLink {
        let lnk = new MenuLink(this.nextId, link, hook, this.store);
        this.__items.push(lnk);
        this.__interactive.push(lnk)
        this.elem.appendChild(lnk.self);
        return lnk;
    }

    imgbox (src: string, hook?: string) {
        let img = new MenuImgbox(this.nextId, src, hook, this.store);
        this.__items.push(img);
        this.elem.appendChild(img.self);
        return img;
    }

    expand (descriptor: MenuExpandDesctiptor, hook?:string) {
        let expand = new MenuExpand(this.nextId, descriptor, this.id + "_epxandgroup", hook, this.store);
        this.__items.push(expand);
        this.__interactive.push(expand);
        this.elem.appendChild(expand.self);
        expand.onedit = data => this.onedit(data);
        return expand;
    }

    textInput (value?: string, placeholder?: string, hook?: string, autofocus?: true): MenuTextInput {
        let input = new MenuTextInput(this.nextId, value || '', placeholder, hook, this.store, autofocus);
        this.__items.push(input);
        // this.__interactive.push(input)
        this.elem.appendChild(input.self);
        return input;
    }

    
    // Selectionne l'élément suivant
    next () {
        if (!this.loaded) return;
        if (!this.__status.interactive.enable) return;
        if (this.__status.interactive.expand) {
            this.__status.interactive.expand.next();
            return;
        }
        this.__status.interactive.select++;
        if (this.__status.interactive.select == this.__interactive.length) 
            this.__status.interactive.select = 0;
        this.__update_hover();
    }

    // Selectionne l'élément précédent
    prev () {
        if (!this.loaded) return;
        if (!this.__status.interactive.enable) return;
        if (this.__status.interactive.expand) {
            this.__status.interactive.expand.prev();
            return;
        }
        this.__status.interactive.select--;
        if (this.__status.interactive.select < 0) 
        this.__status.interactive.select = this.__interactive.length - 1;
        this.__update_hover();
    }

    // Active le mode Hover
    enter () {
        if (!this.loaded) return;
        this.__status.interactive.select = 0;
        this.__status.interactive.enable = true;
        this.__update_hover();
    }

    // Arrête le mode hover
    leave () {
        if (!this.loaded) return;
        if (!this.__status.interactive.enable) {
            return this.onabort();
        };

        if (this.__status.interactive.expand) {
            this.__status.interactive.expand.leave();
            this.__status.interactive.expand = undefined;
            return;
        }
        this.__status.interactive.enable = false;
        this.__update_hover();
    }

    click () {
        if (!this.loaded) return;
        if (!this.__status.interactive.enable) return;

        let item = this.__interactive[this.__status.interactive.select];

        switch (item.type) {

            case "expand":
                    if (this.__status.interactive.expand) 
                        this.__status.interactive.expand.click();
                    else {
                        this.__status.interactive.expand = <MenuExpand>item;
                        this.__status.interactive.expand.enter();
                    }
                break;

            default:
                item.active = false;
        }

    }

    private __update_hover () {
        this.__interactive.forEach((e, i) => e.hover = this.__status.interactive.enable && i == this.__status.interactive.select);
    }

    get id () {
        return this.__status.id;
    }

    get title () {
        return this.__status.title;
    }
    set title (t) {
        this.__status.title = t;
        this.onTitleChange(t);
    }

    get nextId () {
        return `${this.id}_${this.__cpt++}`;
    }

    get interactive () {
        return this.__status.interactive.enable
    }

}