import { callback, Store } from "../store";
import { Menu } from "./menu";

export type MenuFieldType = 
    'title' | 'textbox' | 'imgbox' | 'button' | 'cmd_info' | 'link' | 'expand' | 'textinput' | 'nav';

export class MenuField<V> {

    readonly id: string;
    readonly type: MenuFieldType;

    private readonly hook: string;
    private readonly hook_callback: callback<V>;

    protected old_value: V;
    private __value: V;
    private __inhibit_store_update: boolean = false;
    protected __ready: boolean = false;

    private __disabled: boolean = false;

    protected elem: HTMLElement;

    public onclick: callback<typeof this> = () => {};

    constructor (type: MenuFieldType, id: string, private readonly default_value: V, hook?: string, protected readonly store?: Store, elem?: HTMLElement, force_default_value: boolean = false) {
        this.type = type;
        this.id = id;
        
        this.elem = elem || document.createElement("div");
        this.elem.id = id;
        this.elem.onclick = () => this.onclick(this);

        this.hook = hook || '';
        this.hook_callback = v => { 
            this.__inhibit_store_update = true;
            this.value = v; 
        }

        if (hook && store) {
            store.hook<V>(this.hook, this.hook_callback);
            store.set<V>(this.hook, default_value, !force_default_value);
        }

        this.old_value = default_value;
        this.__value = default_value;
    }

    focus (): void {
        this.old_value = this.value;
    }

    valid (menu: Menu): void {
        this.onValid(menu);
    }

    escape (menu: Menu): void {
        this.value = this.old_value;
        this.onEscape(menu);
    }

    protected __update_html () {
        
    }

    // fonction lors de l'appui
    protected __active () {
        (<() => void>this.elem.onclick)();
    }

    set value (value: V) {  
        this.__value = value;
        if (this.store && !this.__inhibit_store_update) {
            this.__inhibit_store_update = true;
            this.store.set(this.hook, value);
        }
        this.__inhibit_store_update = false;
        if (this.__ready) this.__update_html();
    }

    get value (): V {
        return (this.hook != "" && this.store) ? (this.store.value(this.hook) || this.default_value) : this.__value;
    }

    get disabled (): boolean {
        return this.__disabled;
    }
    set disabled (state: boolean) {
        this.__disabled = state;
        this.__update_html()
    }

    get self () { return this.elem }

    onValid: callback<Menu> = () => {};
    onEscape: callback<Menu> = () => {};

    dismount () {
        this.store?.unhook(this.hook_callback);
    }

    // Ajoute la classe "active" à l'élément et déclenche __active quand active deviens faux
    set active (a: boolean) {
        this.elem.classList.toggle("active", a);
        if (!a && !this.disabled) this.__active();
    }
    get active () {
        return this.elem.classList.contains("active");
    }
    
    set hover (a: boolean) {
        this.elem.classList.toggle("hover", a);
    }
    get hover () {
        return this.elem.classList.contains("hover");
    }

}